// Kafka consumer endpoint
endpoint kafka:SimpleConsumer consumer {
    bootstrapServers: "localhost:9092, 
    // Consumer group ID
    groupId: "student",
    // broadcsated topic 'DistFile'
    topics: ["DistFile"],
    // endpoint polling
    pollingInterval:3000
};

sub.bal
import ballerina/log;
import wso2/kafka;
import ballerina/internal;

//  consumer endpoint node declaration
endpoint kafka:SimpleConsumer consumer {
    bootstrapServers: "localhost:9092",
    // Consumer group student
    groupId: "student",
    // topic 
    topics: ["Distile"],
    // polling interval
    pollingInterval:1000
};

//servic eto replicate the topic
service<kafka:Consumer> kafkaService bind consumer {
    
    onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
        // processing service serializaion
        foreach entry in records {
            byte[] serializedMsg = entry.value;
            
            string st = internal:byteArrayToString(serializedMsg, "UTF-8");
            log:printInfo("New file updat receivde from endpoint");
            // Adding file to database
            log:printInfo("Topic: " + entry.topic + "; Received Message: " +st);
           
            log:printInfo("File file uploaded to database");
        }
    }
}