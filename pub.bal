import ballerina/http;
import wso2/kafka;

//admin credentials
@final string username = "admin";
@final string password = "admin";

// creating client entities
endpoint kafka:SimpleProducer kafkaProducer {
    bootstrapServers: "localhost:9092",
    clientID:"producer",
    acks:"all",
    noRetries:3
};

// HTTP service endpoint
endpoint http:Listener listener {
    port:9090
};

@http:ServiceConfig {basePath:"/Producer"}  //creating remote path
service<http:Service> adminService bind listener {

    @http:ResourceConfig {methods:["POST"], consumes:["application/json"],
        produces:["application/json"]}
    updateFile (endpoint client, http:Request request) {
        http:Response response;
        json reqPayload;
        float newFile;

        //Initiating payload
        match request.getJsonPayload() {
            // Valid 
            json payload => reqPayload = payload;
            // INVALID TEXTING 
            any => {
                response.statusCode = 400;
                response.setJsonPayload({"Message":"Invalid request detected"});
                _ = client -> respond(response);
                done;
            }
        }

          json fileName = reqPayload.File;
        json size = reqPayload.Size;
        json username = reqPayload.Username;
        json password = reqPayload.Password;
      
        if (username == null || password == null || fileName == null || size == null) {
            response.statusCode = 400;
            response.setJsonPayload({"Message":"Bad Request detected"});
            _ = client->respond(response);
            done;
        }

        var result = <float>size.toString();
        match result {
            float value => {
                size = value;
            }
            error err => {
                response.statusCode = 400;
                response.setJsonPayload({"Message":"Invalid file size detected"});
                _ = client->respond(response);
                done;
            }
        }

             // user authentication
               if (username.toString() != username || password.toString() != password) {
            response.statusCode = 403;
            response.setJsonPayload({"Message":"Access Denied"});
            _ = client->respond(response);
            done;
        }

        // Construct and serialize the message to be published to the Kafka topic
        json UpdateInfo = {"File":fileName, "UpdateSize":size};
        byte[] serializedMsg = UpdateInfo.toString().toByteArray("UTF-8");

        // produce msg to broadcasted  topic
        kafkaProducer->send(serializedMsg, "File-Size", partition = 0);
        //sending an ack to admin severs
        response.setJsonPayload({"Status":"Success"});
        _ = client->respond(response);
    }
}